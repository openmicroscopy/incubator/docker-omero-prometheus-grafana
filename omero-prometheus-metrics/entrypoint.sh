#!/bin/sh
exec /opt/omero-prom/venv/bin/python /opt/omero-prom/omero-prometheus-tools/sessions/metrics.py -s "${OMERO_SERVER}" -u "${OMERO_USER}" -w "${OMERO_PASSWORD}" -l 9051
