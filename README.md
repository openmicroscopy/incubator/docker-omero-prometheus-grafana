# OMERO Prometheus Grafana

Docker Compose example of monitoring OMERO using Prometheus and Grafana.

## Quick start

1. Copy `omero-connection.env.template` to `omero-connection.env`.
2. Edit `omero-connection.env` with the connection details of a non-privileged OMERO user that can be used to query OMERO.server for user details.
3. Run `docker-compose up -d`
4. Optionally run `docker-compose logs -f`

Prometheus is listening on port 9090 (no authentication).
Grafana is listening on port 3000 (anonymous view is enabled, to edit the dashboards login with user:`admin` password:`secret`).
